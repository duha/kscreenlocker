# Translation of kcm_screenlocker.po to Catalan (Valencian)
# Copyright (C) 2014-2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2024.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kscreenlocker\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-14 00:39+0000\n"
"PO-Revision-Date: 2024-02-14 10:22+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.3\n"

#: ui/Appearance.qml:22
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Aparença"

#: ui/Appearance.qml:38
#, kde-format
msgid "Wallpaper type:"
msgstr "Tipus de fons de pantalla:"

#: ui/DurationPromptDialog.qml:77
#, kde-format
msgctxt "@action:button"
msgid "Confirm"
msgstr "Confirma"

#: ui/DurationPromptDialog.qml:85
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Cancel·la"

#: ui/DurationPromptDialog.qml:115
#, kde-format
msgctxt "The unit of the time input field"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "minut"
msgstr[1] "minuts"

#: ui/DurationPromptDialog.qml:120
#, kde-format
msgctxt "The unit of the time input field"
msgid "second"
msgid_plural "seconds"
msgstr[0] "segon"
msgstr[1] "segons"

#: ui/DurationPromptDialog.qml:129
#, kde-format
msgctxt "The unit of the time input field"
msgid "minutes"
msgstr "minuts"

#: ui/DurationPromptDialog.qml:136
#, kde-format
msgctxt "The unit of the time input field"
msgid "seconds"
msgstr "segons"

#: ui/main.qml:23
#, kde-format
msgctxt "Screen will not lock automatically"
msgid "Never"
msgstr "Mai"

#: ui/main.qml:24 ui/main.qml:36
#, kde-format
msgid "1 minute"
msgstr "1 minut"

#: ui/main.qml:25 ui/main.qml:37
#, kde-format
msgid "2 minutes"
msgstr "2 minuts"

#: ui/main.qml:26 ui/main.qml:38
#, kde-format
msgid "5 minutes"
msgstr "5 minuts"

#: ui/main.qml:27 ui/main.qml:39
#, kde-format
msgid "10 minutes"
msgstr "10 minuts"

#: ui/main.qml:28 ui/main.qml:40
#, kde-format
msgid "15 minutes"
msgstr "15 minuts"

#: ui/main.qml:29
#, kde-format
msgid "30 minutes"
msgstr "30 minuts"

#: ui/main.qml:30 ui/main.qml:41
#, kde-format
msgctxt "To add a custom value, not in the predefined list"
msgid "Custom"
msgstr "Personalitzat"

#: ui/main.qml:35
#, kde-format
msgctxt "The grace period is disabled"
msgid "Require password immediately"
msgstr "Sol·licita la contrasenya immediatament"

#: ui/main.qml:50
#, kde-format
msgid "Lock screen automatically:"
msgstr "Bloqueja automàticament la pantalla:"

#: ui/main.qml:84 ui/main.qml:165 ui/main.qml:302 ui/main.qml:374
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minut"
msgstr[1] "%1 minuts"

#: ui/main.qml:96
#, kde-format
msgctxt "@option:check"
msgid "Lock after waking from sleep"
msgstr "Bloqueja després de despertar-se"

#: ui/main.qml:112
#, kde-format
msgctxt "First part of sentence \"Delay before password required: X minutes\""
msgid "Delay before password required:"
msgstr "Retard abans de sol·licitar la contrasenya:"

#: ui/main.qml:166 ui/main.qml:376
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 segon"
msgstr[1] "%1 segons"

#: ui/main.qml:184
#, kde-format
msgid "Keyboard shortcut:"
msgstr "Drecera del teclat:"

#: ui/main.qml:199
#, kde-format
msgid "Appearance:"
msgstr "Aparença:"

#: ui/main.qml:200
#, kde-format
msgctxt "@action:button"
msgid "Configure..."
msgstr "Configura..."

#: ui/main.qml:269 ui/main.qml:329
#, kde-format
msgctxt "@title:window"
msgid "Custom Duration"
msgstr "Duració personalitzada"
